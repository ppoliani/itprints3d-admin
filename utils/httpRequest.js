// region Import

var http = require('http'),
    Q    = require('q');

// endregion

// region Inner Methods

function request(options){
    var defer = Q.defer(),
        data = '';

    var req = http.request(options, function(res){
        res.on('data', function(chunk){
            data += chunk;
        });

        res.on('end', function(){
            defer.resolve(data);
            req.end();
        });
    })
    .on('error', function(err){
        defer.reject(err);
        req.end();
    });

    req.end();

    return defer.promise;
}

// endregion

// region Export

module.exports = {
    request: request
};

// endregion