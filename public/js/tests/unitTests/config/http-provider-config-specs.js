
describe('The http-provider-config module', function(){

    var
        app = require('../../../app/core/app.js'),
        $httpBackend,
        $http;

       beforeEach(function(){
           var tmpHttpInterceptor = function($q){
               return {
                   request: function request(config) {
                       // check if the previous interceptor has added the access token
                       expect(config.headers.Authorization).toBe('Bearer 1234');

                       return config;
                   },

                   response: function(response){
                       return response;
                   }
               };
           };

           app.factory('tmpHttpInterceptor', [ '$q', '$window', tmpHttpInterceptor]);

           app.config(['$httpProvider', function($httpProvider){
               $httpProvider.interceptors.push('tmpHttpInterceptor');
           }]);

       });

        // Do not use module!! conflicts with webpack module variable
        beforeEach(angular.mock.module('app.core'));

        beforeEach(inject(function($injector){
            $httpBackend = $injector.get('$httpBackend');
            $http = $injector.get('$http');
        }));


    it('Should register a oauth http interceptor that adds a bearer header to the outgoing request', inject(function($window){
        $window.sessionStorage.access_token = '1234';

        $httpBackend.expectGET('/users').respond({});
        $http.get('/users')
            .success(function(){
            })
            .error(function(){
            });
        $httpBackend.flush();
    }));
});