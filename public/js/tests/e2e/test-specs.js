describe('angularjs homepage', function() {
    var BASE_URL = 'http://localhost:8888';

    beforeEach(function(){
        var ptor = protractor.getInstance();

        browser.get(BASE_URL + '/login');
        element(by.id('user-name')).sendKeys('pp6g11@gmail.com');
        element(by.id('password')).sendKeys('admin');

        element(by.id('login-btn')).click();

        ptor.sleep(5000);
    });

    it('Should remove the parts that the admin is not eligible to view', function() {
        browser.get(BASE_URL + '/people');

        // ToDo: throws an exception
        var element = element(by.css('.restricted'));
        expect(element).toBe('admin-only');
    });
});
