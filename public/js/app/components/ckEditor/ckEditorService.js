/**
 * A auxiliary service for exchanging data with the skEditor directive
 */
 (function(){
    'use strict';

    function ckEditorService(){
        // region Inner Fields

        var _editors = {};

        // endregion

        // region Inner Methods

        /**
         * Checks if the CKEditor library was loaded
         * @constructor
         */
        function ckEditorLoaded(){
            return CKEDITOR;
        }

        /**
         * Returns editor instance
         * @param editorId
         */
        function getEditor(editorId){
            if(ckEditorLoaded()){
                return CKEDITOR.instances[editorId];
            }
        }

        // endregion

        // region Public API

        return {
            ckEditorLoaded: ckEditorLoaded,
            getEditor: getEditor
        };

        // endregion
    }

    // region CommonJS

    module.exports = {
        name: 'ckEditorService',
        type: 'factory',
        service: [ckEditorService]
    };

    // endregion

 })();