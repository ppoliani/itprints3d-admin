/**
 * Replaces the element with a ckeditor
 */
 (function(){
    'use strict';

    function ckeditorDirective(ngDialog, ckEditorService, IMAGE_URL){
        // region Inner Methods

       function compile(element, attrs){
            if (ckEditorService.ckEditorLoaded()) {
                var editor = CKEDITOR.replace(attrs.id, {
                    filebrowserBrowseUrl : 'http://localhost:9999/api/v1/browser/browse/type/all',
                    filebrowserUploadUrl : 'http://localhost:9999/api/v1/browser/upload/type/all',
                    filebrowserImageBrowseUrl : 'http://localhost:9999/api/v1/browser/browse/type/image',
                    filebrowserImageUploadUrl : 'http://localhost:9999/api/v1/browser/upload/type/image',
                    filebrowserWindowWidth  : 800,
                    filebrowserWindowHeight : 500
                });

                editor.config.fileBrowserCallback = function(clb){
                    var dialog = ngDialog.open({
                        controller: 'imagesCtrl',
                        template: '/js/app/images/images.html',
                        data: {
                            isPopupMode: true
                        }
                    });

                    dialog.closePromise
                        .then(function(url){
                            if(url.value && url.value[0] !== '$'){
                                clb(IMAGE_URL + url.value);
                            }
                        });
                };
            } else{
                setTimeout(compile.bind(null, element, attrs), 100);
            }
        }

        // endregion

        return {
            restrict: 'AE',
            compile: compile
        };
    }

    // region CommonJS

    module.exports = {
        name: 'ckeditor',
        directive: [
            'ngDialog',
            'ckEditorService',
            'IMAGE_URL',
            ckeditorDirective
        ]
    };

    // endregion

 })();