/**
 * Foundation pagination directive
 */
 (function(angular){
    'use strict';

    function paginationDirective(){
        // region Inner Fields

        var paginationConfig = {
            itemsPerPage: 10,
            boundaryLinks: false,
            directionLinks: true,
            firstText: 'First',
            previousText: 'Previous',
            nextText: 'Next',
            lastText: 'Last',
            rotate: true
        };

        // endregion

        // region Inner Methods

        function linkFn(scope, element, attrs, paginationCtrl) {
            paginationCtrl.init(attrs);
            return paginationCtrl.render();
        }

        function init(ctrlAttrs){
            this.init_config(ctrlAttrs);
            this.init_watchers(ctrlAttrs);
            this.init_scope_bindings();
            return this.setNumPages = ctrlAttrs.numPages ? this.$parse(ctrlAttrs.numPages).assign : angular.noop;
        }

        function init_scope_bindings(){
            return this.$scope.selectPage = this.selectPage;
        }

        function init_config (ctrlAttrs){
            this.itemsPerPage = this.$scope.itemsPerPage || this.defaultConfig.itemsPerPage;
            this.boundaryLinks = this.getAttributeValue(ctrlAttrs.boundaryLinks, this.defaultConfig.boundaryLinks);
            this.directionLinks = this.getAttributeValue(ctrlAttrs.directionLinks, this.defaultConfig.directionLinks);
            this.firstText = this.getAttributeValue(ctrlAttrs.firstText, this.defaultConfig.firstText, true);
            this.previousText = this.getAttributeValue(ctrlAttrs.previousText, this.defaultConfig.previousText, true);
            this.nextText = this.getAttributeValue(ctrlAttrs.nextText, this.defaultConfig.nextText, true);
            this.lastText = this.getAttributeValue(ctrlAttrs.lastText, this.defaultConfig.lastText, true);
            this.rotate = this.getAttributeValue(ctrlAttrs.rotate, this.defaultConfig.rotate);
            this.maxSize = this.getAttributeValue(ctrlAttrs.maxSize, null);
            return this.currentPage = this.getAttributeValue(ctrlAttrs.currentPage, 1);
        }

        function init_watchers(ctrlAttrs){
            var _this = this;
            this.$scope.$watch('currentPage', function() {
                return _this.render();
            });
            this.$scope.$watch('totalItems', function() {
                return _this.$scope.totalPages = _this.calculateTotalPages();
            });
            this.$scope.$watch('totalPages', function(value) {
                _this.setNumPages(_this.$scope.$parent, value);
                if (_this.$scope.currentPage > _this.$scope.totalPages) {
                    _this.$scope.currentPage = _this.$scope.totalPages;
                }
                return _this.render();
            });
            if (ctrlAttrs.itemsPerPage) {
                this.$scope.$parent.$watch(this.$parse(ctrlAttrs.itemsPerPage), function(value) {
                    _this.itemsPerPage = parseInt(value, 10);
                    return _this.$scope.totalPages = _this.calculateTotalPages();
                });
            }
            if (ctrlAttrs.maxSize) {
                return this.$scope.$parent.$watch(this.$parse(ctrlAttrs.maxSize), function(value) {
                    _this.maxSize = parseInt(value, 10);
                    return _this.render();
                });
            }
        }

        function getAttributeValue (attribute, defaultValue, interpolate){
            if (angular.isDefined(attribute)) {
                if (interpolate) {
                    return this.$interpolate(attribute)(this.$scope.$parent);
                } else {
                    return this.$scope.$parent.$eval(attribute);
                }
            } else {
                return defaultValue;
            }
        }

        function calculateTotalPages (){
            var totalPages;

            totalPages = this.itemsPerPage < 1 ? 1 : Math.ceil(this.$scope.totalItems / this.itemsPerPage);
            return Math.max(totalPages || 0, 1);
        }

        function render (){
            this.current_page = parseInt(this.$scope.currentPage, 10) || 1;
            this.$scope.totalPages = parseInt(this.$scope.totalPages, 10) || 1;
            if (this.current_page > 0) {
                return this.$scope.pages = this.getPages(this.current_page, this.$scope.totalPages);
            }
        }

        function noPrevious (){
            return this.current_page === 1;
        }

        function noNext (){
            return this.current_page === this.$scope.totalPages;
        }

        function isActive(pageNumber){
            return this.current_page === pageNumber;
        }

        function makePage (number, text, isActive, isDisabled){
            return {
                number: number,
                text: text,
                active: isActive,
                disabled: isDisabled
            };
        }

        function getPages (currentPage, totalPages){
            var endPage, firstPage, isMaxSized, lastPage, nextPage, nextPageSet, number, pages, previousPage, previousPageSet, startPage, _i;
            pages = [];
            startPage = 1;
            endPage = totalPages;
            isMaxSized = this.maxSize !== null && this.maxSize < totalPages;
            if (isMaxSized) {
                if (this.rotate) {
                    startPage = Math.max(currentPage - Math.floor(this.maxSize / 2), 1);
                    endPage = startPage + this.maxSize - 1;
                    if (endPage > totalPages) {
                        endPage = totalPages;
                        startPage = endPage - this.maxSize + 1;
                    }
                } else {
                    startPage = ((Math.ceil(currentPage / this.maxSize) - 1) * this.maxSize) + 1;
                    endPage = Math.min(startPage + this.maxSize - 1, totalPages);
                }
            }
            if (startPage <= endPage) {
                for (number = _i = startPage; startPage <= endPage ? _i <= endPage : _i >= endPage; number = startPage <= endPage ? ++_i : --_i) {
                    pages.push(this.makePage(number, number, this.isActive(number), false));
                }
            }
            if (isMaxSized && !this.rotate) {
                if (startPage > 1) {
                    previousPageSet = this.makePage(startPage - 1, '...', false, false);
                    pages.unshift(previousPageSet);
                }
                if (endPage < totalPages) {
                    nextPageSet = this.makePage(endPage + 1, '...', false, false);
                    pages.push(nextPageSet);
                }
            }
            if (this.directionLinks) {
                previousPage = this.makePage(currentPage - 1, this.previousText, false, this.noPrevious());
                pages.unshift(previousPage);
                nextPage = this.makePage(currentPage + 1, this.nextText, false, this.noNext());
                pages.push(nextPage);
            }
            if (this.boundaryLinks) {
                firstPage = this.makePage(1, this.firstText, false, this.noPrevious());
                pages.unshift(firstPage);
                lastPage = this.makePage(totalPages, this.lastText, false, this.noNext());
                pages.push(lastPage);
            }
            return pages;
        }

        function selectPage (pageNumber){
            if (this.isActive && pageNumber > 0 && pageNumber <= this.$scope.totalPages) {
                this.$scope.currentPage = pageNumber;
                return this.$scope.onSelectPage({
                    page: pageNumber
                });
            }
        }

        // endregion

        // region Controller

        function PaginationController($scope, $attrs, $interpolate, $parse) {
                this.$scope = $scope;
                this.$attrs = $attrs;
                this.$interpolate = $interpolate;
                this.$parse = $parse;
                this.defaultConfig = paginationConfig;
                this.selectPage = this.selectPage.bind(this);
        }

        PaginationController.prototype = {
            constructor: PaginationController,
            init: init,
            init_scope_bindings: init_scope_bindings,
            init_config: init_config,
            init_watchers: init_watchers,
            getAttributeValue: getAttributeValue,
            calculateTotalPages: calculateTotalPages,
            render: render,
            noPrevious: noPrevious,
            noNext: noNext,
            isActive: isActive,
            makePage: makePage,
            getPages: getPages,
            selectPage: selectPage
        };

        // endregion

        return {
            restrict: 'AE',
            controller: [
                '$scope',
                '$attrs',
                '$interpolate',
                '$parse',
                PaginationController
            ],
            scope: {
                currentPage: '=',
                totalItems: '=',
                itemsPerPage: '=',
                onSelectPage: '&'
            },
            template: '<ul class="pagination">\n  <li ng-repeat="page in pages" ng-class="{current: page.active, unavailable: page.disabled}">\n <a ng-click="selectPage(page.number)">{{page.text}}</a>\n  </li>\n</ul>',
            replace: true,
            link: linkFn
        };
    }

    // region CommonJS

    module.exports = {
        name: 'pagination',
        directive: [paginationDirective]
    };

    // endregion

 })(window.angular);