/**
 * A directive that will be populated by the back-end
 * with the preloaded data
 */
(function(){
    'use strict';

    function preloadResourceDirective($rootScope) {

        // region Inner Methods

        function linkFn(scope, element, attrs){
            $rootScope.breezeMetadata = attrs.resourceContent;
            element.remove();
        }

        // endregion

        return {
            restrict: 'AE',
            link: linkFn
        };
    }

    // region CommonJS

    module.exports = {
        name: 'preloadResource',
        directive: ['$rootScope', preloadResourceDirective]
    };

    // endregion
})();