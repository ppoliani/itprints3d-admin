/**
 * A directive that triggers the breeze initialization process, this is used because we
 * want the rootScope to be populated with the metadata before the init process starts
 */
 (function(){
    'use strict';

    function breezeInitDirective(breezeService){

        // region Inner Methods

        function linkFn(scope, element){
            breezeService.init();
            element.remove();
        }

        // endregion

        return {
            restrict: 'AE',
            link: linkFn
        };
    }

    // region CommonJS

    module.exports = {
        name: 'breezeInit',
        directive: ['breezeService', breezeInitDirective]
    };

    // endregion

 })();