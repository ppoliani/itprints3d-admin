/**
 * Authentication controller
 */
(function(){
    'use strict';

    function authCtrl(authService){
        // region Inner Methods
        /**
         * Will call the login method of the auth service
         */
        function login(){
            authService.login(this.username, this.password);
        }

        // endregion

        authService.checkIfLoggedIn();

        // region Viewmodel

        this.username = '';
        this.password = '';

        this.login = login;

        // endregion
    }

    module.exports = {
        name: 'authCtrl',
        ctrl: ['authService', authCtrl]
    };
})();