/**
 * Header ctrl
 */
(function () {
    'use strict';

    function headerCtrl() {
        // region Inner fields

        // endregion

        // region Inner Methods

        // endregion

        // region Viewmodel

        this.isMenuVisible = false;

        /**
         * Toggles the visibility of the menu
         */
        this.toggleMenu = function toggleMenu(){
            this.isMenuVisible = !this.isMenuVisible;
        };

        // endregion
    }

    // region CommonJS

    module.exports = {
        name: 'headerCtrl',
        ctrl: [ headerCtrl]
    };

    // endregion

})();