/**
 * Loads all the controllers of the app
 */
module.exports = [
    require('../partial/header/headerCtrl'),
    require('../home/homeCtrl'),
    require('../auth/authCtrl'),

    require('../printers/printerCtrl'),
    require('../printers/addEditPrinterCtrl'),

    require('../images/imagesCtrl'),
    require('../images/uploadImageCtrl')
];