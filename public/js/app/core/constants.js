/**
 * Contains all the constants
 */
module.exports = [{
    name: 'SERVICE_ENDPOINT',
    type: 'constant',
    service: '@@SERVICE_ENDPOINT'
}, {
    name: 'TOKEN_URL',
    type: 'constant',
    service: '@@TOKEN_URL'
}, {
    name: 'IMAGE_URL',
    type: 'constant',
    service: '@@IMAGE_URL'
}];