/**
 * Contains all the value services that will be used in the app
 */
module.exports = [{
    name: 'alertify',
    type: 'value',
    service: alertify
}];