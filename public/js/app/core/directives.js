/**
 * Loads all the directives
 */
module.exports = [
    require('../components/preloadResource/preloadResource'),
    require('../components/breezeInit/breezeInit'),
    require('../components/ckEditor/ckeditor'),
    require('../components/imgThumb/imgThumb'),
    require('../components/pagination/pagination'),
    require('../components/validationError/validationError')
];