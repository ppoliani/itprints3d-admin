/**
 * Loads all the services of the app
 */
module.exports = [
    require('../auth/authService'),
    require('../auth/routeAuthService'),
    require('../data-services/resourceService'),
    require('../utils/loggerProvider'),
    require('../utils/pubsub'),

    // Data
    require('../data/breezeService'),
    require('../data/jsonResultsAdapterService'),
    require('../data/models/modelSet'),
    require('../data/repositoryQueryService'),
    require('../data/repositoryService'),
    require('../data/repositoriesService'),
    require('../data/unitOfWorkService'),

    require('../components/ckEditor/ckEditorService')
];