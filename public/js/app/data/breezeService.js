/**
 * A service that initializes breezejs
 */
(function () {
    'use strict';

    function breezeService($rootScope, $http, breeze, jsonResultsAdapterService, modelSet, logger, SERVICE_ENDPOINT) {

        // region Consts

        var METADATA_ENDPOINT = SERVICE_ENDPOINT + 'breeze/metadata';

        // endregion

        // region Inner Fields

        var
            _entityManager,
            _metadata;

        // endregion

        // region Inner Methods

        /**
         * Performs initialization tasks
         */
        function init(){
            if($rootScope.breezeMetadata){
                _metadata = $rootScope.breezeMetadata;
                _configBreeze();
            }
            else {
                _readMetadata()
                    .success(function(metadata){
                        logger.info('Metadata Loaded');
                        _metadata = metadata;
                        _configBreeze();
                    })
                    .error(function(msg){
                        logger.error('An error occured' + msg);
                    });
            }
        }

        /**
         * Creates a new entity of the given type
         * @param type
         * @param value
         */
        function createEntity(type, value){
            if(!_metadata){
                throw new Error('Metadata has not been loaded yet!');
            }

            return _entityManager.createEntity(type, value);
        }

        /**
         * Fetches the metadata from the back-end
         * @private
         */
        function _readMetadata(){
            return $http.get(METADATA_ENDPOINT);
        }

        /**
         * Starts the configuration process
         * @private
         */
        function _configBreeze(){
            _customizeAjaxAdapter();
            _entityManager = _createEntityManager();
            _registerCustomCtors();
        }

        /**
         * Customizes the breezeJS ajax adapter
         * @private
         */
        function _customizeAjaxAdapter(){
            var ajax = breeze.config.initializeAdapterInstance('ajax', 'angular');

            ajax.setHttp($http);
        }

        /**
         * Creates a new breezeJS entity manager
         * @private
         */
        function _createEntityManager(){
            breeze.config.initializeAdapterInstance("modelLibrary", "backingStore", true);

            // set custom naming convention
            _getCustomNamingConvention().setAsDefault();

            // define the breeze DataService
            var
                dataService = new breeze.DataService({
                    serviceName: SERVICE_ENDPOINT,
                    hasServerMetadata: false, // don't ask the server for metadata; we'll inject them manually
                    jsonResultsAdapter: jsonResultsAdapterService
                }),

                // create the metadataStore
                metadataStore = new breeze.MetadataStore();

            // import metadata
            metadataStore.importMetadata(_metadata);

            // create a new EntityManager that uses this metadataStore
            return new breeze.EntityManager({
                dataService: dataService,
                metadataStore: metadataStore
            });
        }

        /**
         * Registers custom constructors for our models
         * @private
         */
        function _registerCustomCtors(){
            var store = _entityManager.metadataStore;

            for(var model in modelSet){
                store.registerEntityTypeCtor(model, modelSet[model]);
            }
        }

        /**
         * Creates a custom naming convention
         * @private
         */
         function _getCustomNamingConvention(){
            var toCamelCase = function(propertyName){
                return propertyName.charAt(0).toLowerCase() + propertyName.slice(1);
            };

            return new breeze.NamingConvention({
                name: 'camelCaseOnBothSides',
                serverPropertyNameToClient: toCamelCase,
                clientPropertyNameToServer: toCamelCase
            });
        }

        // endregion

        // region Public API

        var publicApi =  {
            init: init,
            createEntity: createEntity
        };

        Object.defineProperty(publicApi, 'entityManager', {
            get: function get() {
                return _entityManager;
            }
        });

        Object.defineProperty(publicApi, 'metadataLoaded', {
            get: function get(){
                return _metadata !== undefined;
            }
        });

        return publicApi;

        // endregion
    }

    // region CommonJS

    module.exports = {
        name: 'breezeService',
        type: 'factory',
        service: [
            '$rootScope',
            '$http',
            'breeze',
            'jsonResultsAdapterService',
            'modelSet',
            'logger',
            'SERVICE_ENDPOINT',
            breezeService
        ]
    };

    // endregion

})();