/**
 * Aggregates all the models as angular dependencies
 */
(function(){
    'use strict';

    function modelSet(Printer){
        return {
            Printer: Printer
        };
    }

    // region CommonJS

    module.exports = {
        name: 'modelSet',
        type: 'factory',
        service: [
            'Printer',
            modelSet
        ]
    };

    // endregion
})();