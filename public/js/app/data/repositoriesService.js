/**
 * A collection of all the repositories in the application
 */
(function () {
    'use strict';

    function RepositoriesService(RepositoryService) {

        // region Inner Fields

        var Repository = RepositoryService.Repository;

        // endregion

        // region Public API

        return {
            printerRepository: new Repository('Printer', 'printers', 'admin/printers/lookups'),
            imageRepository: new Repository('Printer_Image', 'images')
        };

        // endregion
    }

    // region CommonJS

    module.exports = {
        name: 'RepositoriesService',
        type: 'factory',
        service: ['RepositoryService', RepositoriesService]
    };

    // endregion

})();