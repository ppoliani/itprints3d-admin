/**
 * Printer page controller
 */
 (function(){
    'use strict';

    function printerCtrl($state, IMAGE_URL, logger, unitOfWork){
        // region Inner Fields

        var _printerRepository = unitOfWork.repository('printer'),
            itemsPerPage = 5;

        // endregion

        // region Inner Methods

        /**
         * Loads all the printers for this user
         * @private
         */
        function _loadData(){
            _printerRepository.query()
                .lookups()
                .skip((this.currentPage - 1) * itemsPerPage)
                .take(itemsPerPage)
                .exec()
                .then(function(printers){
                    this.printers = printers.results;
                    this.totalItems = printers.httpResponse.data.total;
                }.bind(this));
        }

        // endregion

        // region Viewmodel

        this.image_url = IMAGE_URL;

        this.itemsPerPage = 5;
        this.totalItems = undefined;
        this.currentPage = 1;

        /**
         * Navigates to the edit printer page
         * @param printerId
         */
        this.editPrinter = function editPrinter(printerId){
            $state.go('printers.edit', { id: printerId });
        };

        /**
         * Removs the given printer
         * @param printerId
         * @param index
         */
        this.removePrinter = function removePrinter(printerId, index){
            _printerRepository.query()
                .setResourceId(printerId)
                .exec()
                .then(function(printer){
                    _printerRepository.deleteEntity(printer.results[0]);

                    unitOfWork.save()
                        .then(function(){
                            logger.success('Printer removed');

                            this.printers.splice(index, 1);
                        }.bind(this))
                        .catch(function(err){
                            logger.error('Error deleting the given printer: ' + err);
                        });
                }.bind(this))
                .catch(function(err){
                    logger.error('Error deleting the given printer: ' + err);
                });
        };

        /**
         * Invoked when a page is selected (i.e. pagination)
         * @param page
         */
        this.onSelectPage = function onSelectPage(page){
            this.currentPage = page;
            _loadData.call(this);
        };

        // endregion

        _loadData.call(this);
    }

    // region CommonJS

    module.exports = {
        name: 'printerCtrl',
        ctrl: [
            '$state',
            'IMAGE_URL',
            'logger',
            'UnitOfWorkService',
            printerCtrl
        ]
    };

    // endregion

 })();
