/**
 * Add edit pritner controller
 */
 (function(){
    'use strict';

    function addEditPrinterCtrl($stateParams, ngDialog, IMAGE_URL, unitOfWork, logger, ckEditorService, authService){
        // region Inner Fields

        var _printerRepository = unitOfWork.repository('printer'),
            _mode = 'add';

        // endregion

        // region Inner Methods

        /**
         * First function to be called
         * @private
         */
        function _init(){
            if($stateParams.id){
                _loadPrinter.call(this);
                this.btnText = 'edit';
                _mode = 'edit';
            }
        }

        /**
         * Sets the CKEditor content
         * @param content
         * @private
         */
        function _setCKEditorContent(content){
            var ckEditor = ckEditorService.getEditor('description');
            if(ckEditor){
                ckEditor.setData(content, function(){
                    // just to make sure it always works
                    ckEditor.setData(content);
                });
            } else {
                setTimeout(_setCKEditorContent.bind(null, content),0);
            }
        }

        /**
         * Will load printer info if we are in edit mode
         * @private
         */
        function _loadPrinter(){
            _printerRepository.query()
                .setResourceId($stateParams.id)
                .exec()
                .then(function(printer){
                    this.printer = printer.results[0];
                    this.images = this.printer.printer_ImageID;

                    _setCKEditorContent(this.printer.description.trim());
                }.bind(this))
                .catch(function(err){
                    logger.error('Could not load printer data: ' + err);
                });
        }

        // endregion

        // region Viewmodel

        this.printer;

        this.image_url = IMAGE_URL;

        this.btnText = 'add';

        /**
         * Adds a new printer
         */
        this.addPrinter =  function addPrinter(isValid){
            if(isValid){
                this.printer.description = ckEditorService.getEditor('description').getData();
                this.printer.userID = authService.getUserInfo().id;

                if(!this.printer.printer_ImageID){
                    this.printer.printer_ImageID = [];
                }

                var printerEntity = _mode === 'add'
                    ? _printerRepository.attach(this.printer)
                    : _printerRepository.update(this.printer);

                if(this.images) {
                    this.images.forEach(function (image, index) {
                        printerEntity.printer_ImageID[index] = image;
                    });
                }

                unitOfWork.save()
                    .then(function(){
                        logger.success('Successfull save');
                        this.printer = null;
                    }.bind(this))
                    .catch(function(message){
                        logger.success('An error occurred: ' + message);
                    });
            }
        };

        /**
         * Adds images for this printer
         */
        this.addImages = function addImages(){
            var dialog = ngDialog.open({
                controller: 'imagesCtrl as vm',
                template: '/js/app/images/images.html',
                data: {
                    isPopupMode: true
                }
            });

            dialog.closePromise
                .then(function(images){
                    if(Array.isArray(images.value)){
                        images.value.forEach(function(image){
                            this.images.push(image);
                        }.bind(this));
                    }
                }.bind(this));
        };

        /**
         * Removes the given image from the list of selected images
         * @param imageId
         */
        this.removeSelectedImage = function removeSelectedImage(imageId){
            var index = this.images.indexOf(imageId);

            if(index !== -1){
                this.images.splice(index, 1);
            }
        };

        // endregion

        _init.call(this);
    }

    // region CommonJS

    module.exports = {
        name: 'addEditPrinterCtrl',
        ctrl: [
            '$stateParams',
            'ngDialog',
            'IMAGE_URL',
            'UnitOfWorkService',
            'logger',
            'ckEditorService',
            'authService',
            addEditPrinterCtrl
        ]
    };

    // endregion

 })();
