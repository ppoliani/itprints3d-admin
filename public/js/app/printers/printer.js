 /**
  * Printer model
  */
  (function(){
     'use strict';

     function PrinterModel(){

         // region Inner Methods

         // endregion

         // region Ctor

         var Printer = function Printer(){
             this.id = undefined;
             this.brandName = undefined;
             this.productName = undefined;
             this.description = undefined;
             this.weight = undefined;
             this.dimension = null;
             this.layerThickness = undefined;
             this.powerRequirements = undefined;
             this.warranty = undefined;
             this.connectivity = null;
             this.buildVolume = null;
             this.printingTechnology = undefined;
             this.supportMaterial = undefined;
             this.extraFeatures = undefined;
             this.userID = undefined;
             this.printer_ImageID = undefined;
         };

         Printer.prototype = (function(){

             // region Public Api

             var publicAPI = {
                 constructor: Printer
             };

             // endregion

             /* test-code */

             /* end-test-code */

             return publicAPI;

         })();

         // endregion

         return Printer;
     }

     // region CommonJS

     module.exports = {
         name: 'Printer',
         ctor: [PrinterModel]
     };

     // endregion

  })();