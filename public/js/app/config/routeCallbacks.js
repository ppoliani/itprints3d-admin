/**
 * Defines callbacks for the route events
 */
var routeCallbackConfig = (function(){
    'use strict';

    function configure (app){
        app.run(['$rootScope', '$window', '$state', '$timeout', 'breezeService', 'routeAuthService', function($rootScope, $window, $state, $timeout, breezeService, routeAuthService){
            $rootScope.$on('$stateChangeStart', function(event, toState){
                if(!breezeService.metadataLoaded) {
                    event.preventDefault();

                    // prevent state transition if metadata not loaded
                    var metadataLoaded = function () {
                        if (breezeService.metadataLoaded) {
                            $state.go(toState.name)
                        } else {
                            $timeout(metadataLoaded, 0);
                        }
                    };

                    $timeout(metadataLoaded, 0);
                }

                // if user unauthorized
                if(!routeAuthService.canVisitRoute(toState)){
                    event.preventDefault();
                    $state.go('home');
                }
            });

            $rootScope.$on('$stateChangeSuccess', function(event, toState){
                $rootScope.title = 'ItPrints3d | ' + toState.name;
            });
        }]);
    }

    return {
        configure: configure
    };
})();

// region CommonJS

module.exports = routeCallbackConfig;

// endregion