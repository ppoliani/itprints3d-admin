/**
 * App entry point
 */
var httpProviderConfig = (function(){
    'use strict';

    // region Consts

    // This is relative to the nodeJS static files directory !!!
    var BASE_DIR = '/js/app/';

    // endregion

    // region Inner Methods

    /**
     * Runs the configurations
     */
     function configure(app){
        app.config(['$stateProvider',
            '$urlRouterProvider',
            '$locationProvider', function($stateProvider, $urlRouterProvider, $locationProvider){
                $stateProvider
                    .state('home', {
                        url: '/',
                        templateUrl: _getPath('home/index'),
                        controller: 'homeCtrl as vm'
                    })

                    .state('login', {
                        url: '/login',
                        templateUrl: _getPath('auth/login'),
                        controller: 'authCtrl as vm'
                    })

                    .state('printers', {
                        abstract: true,
                        templateUrl: _getPath('printers/printers'),
                        controller: 'printerCtrl as vm',
                        claims: {
                            role: ['admin']
                        }
                    })

                    .state('printers.list', {
                        url: '/printers',
                        templateUrl: _getPath('printers/printer-list'),
                        claims: {
                            role: ['admin']
                        }
                    })

                    .state('printers.add', {
                        url: '/printers/new',
                        templateUrl: _getPath('printers/add-edit-printer'),
                        controller: 'addEditPrinterCtrl as vm',
                        claims: {
                            role: ['admin']
                        }
                    })

                    .state('printers.edit', {
                        url: '/printers/:id',
                        templateUrl: _getPath('printers/add-edit-printer'),
                        controller: 'addEditPrinterCtrl as vm',
                        claims: {
                            role: ['admin']
                        }
                    })

                    .state('images', {
                        url: '/images',
                        templateUrl: _getPath('images/images'),
                        controller: 'imagesCtrl as vm',
                        claims: {
                            role: ['admin']
                        }
                    });

                $urlRouterProvider.otherwise('/');
                $locationProvider.html5Mode(true);
            }]);
    }

    /**
     * Return the paths relative to base dir
     *
     * @param filePath
     * @returns {string}
     * @private
     */
     function _getPath(filePath){
        return BASE_DIR + filePath + '.html';
    }

    // endregion

    // region Public API

    return {
        configure: configure
    };

    // endregion
})();

// region Exports

module.exports = httpProviderConfig;

// endregion