/**
 * Image controller
 */
 (function(){
    'use strict';

    function imagesCtrl($scope, unitOfWork, logger){
        // region Inner Fields

        var _imageRepository = unitOfWork.repository('image'),
            pagesVisited = [1],
            itemsPerPage = 5,
            selectedImages = [];

        //endregion

        // region Inner Methods

        /**
         * Loads all the images for this user
         * @private
         */
        function _loadImages(local){
            var q = _imageRepository.query()
                .skip((this.currentPage - 1) * itemsPerPage)
                .take(itemsPerPage);

            if(!local){
                q.exec()
                    .then(function(images){
                        this.images = images.results;
                        this.totalItems = images.httpResponse.data.total;
                    }.bind(this))
                    .catch(function(err){
                        throw err;
                    });
            } else{
                this.images = [];
                this.images = q.local()
                    .exec();
            }

        }

        // endregion

        // region Viewmodel

        this.images= null;

        this.itemsPerPage = itemsPerPage;
        this.totalItems = undefined;
        this.currentPage = 1;

        /**
         * Invoked when a page is selected (i.e. pagination)
         * @param page
         */
        this.onSelectPage = function onSelectPage(page){
            this.currentPage = page;

            if(pagesVisited.indexOf(page) === -1){
                _loadImages.call(this);
            } else{
                // images are already in the cache
                _loadImages.call(this, true);
            }

            pagesVisited.push(page);
        };

        /***
         * Selects the given image
         * @param image
         */
        this.selectImage = function selectImage(imageId){
            var index = selectedImages.indexOf(imageId);

            if(index === -1){
                selectedImages.push(imageId);
            } else{
                selectedImages.splice(index, 1);
            }
        };

        /**
         * Closes the ngDialog
         */
        this.closeDialog = function closeDialog(){
            $scope.closeThisDialog(selectedImages);
        };

        /**
         * Checks if the fiven image is selected
         * @param id
         * @returns {boolean}
         */
        this.isImageSelected = function isImageSelected(id){
            return selectedImages.indexOf(id) !== -1;
        };

        /**
         * Removes the selected image from the database
         * @param imageId
         */
        this.deleteSelectedImage = function deleteSelectedImage(image, index){
            logger.confirm("Are you sure you want to delete the given image?")
                .then(function(yes){
                    if(yes){
                        _imageRepository.deleteEntity(image);

                        return unitOfWork.save();
                    }
                })
                .then(function(){
                    logger.success('Image removed');

                    this.images.splice(index, 1);
                }.bind(this))
                .catch(function(err){
                    logger.error('An error occured while removing the image');
                });
        };

        // endregion

        _loadImages.call(this);
    }

    // region CommonJS

    module.exports = {
        name: 'imagesCtrl',
        ctrl: [
            '$scope',
            'UnitOfWorkService',
            'logger',
            imagesCtrl
        ]
    };

    // endregion

 })();
