/**
 * Home Controller
 */
(function(){
    'use strict';

    // region Controller

    function homeCtrl(){
        this.title = 'Hello Main Controller';
    }

    // endregion

    // region CommonJS

    module.exports = {
        name: 'homeCtrl',
        ctrl: [homeCtrl]
    };

    // endregion
})();

