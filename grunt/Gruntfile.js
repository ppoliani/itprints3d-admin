﻿
module.exports = function (grunt) {
    require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks);

    var config = {
        distDir: '../public/dist',

        libsDir: '../public/js/third-party',

        jsDir: '../public/js',

        cssDir: '../public/content/css',

        appDir: '../public/js/app',

        testDir: '../public/js/tests/**/*.*',

        // read the package.json file
        pkg: grunt.file.readJSON('package.json'),

        env: process.env
    };

    grunt.util._.extend(config, loadConfig('./build_tasks/'));

    grunt.initConfig(config);

    function loadConfig(path) {
        var glob = require('glob');
        var object = {};
        var key;

        glob.sync('*', {cwd: path}).forEach(function(option) {
            key = option.replace(/\.js$/,'');
            object[key] = require(path + option);
        });

        return object;
    }

    grunt.registerTask('test', ['clean',  'karma:dev']);
    grunt.registerTask('build', ['clean:dist', 'webpack:dev', 'replace:dev', 'concat:vendor', 'sass:app', 'sass:vendor', 'cssmin:vendor', 'copy:dev']);
    grunt.registerTask('dev', ['clean:dist', 'build', 'watch:dev']);
    grunt.registerTask('watchTasks', ['webpack:dev', 'replace:dev', 'sass:app']);
    grunt.registerTask('buildCss', ['clean', 'sass:vendor', 'cssmin:vendor', 'clean:sass']);
};