var config = require('../config');

module.exports = {
    dev: {
        files: [{
            expand: true,
            src: config.copyFiles.fontAwesomeFonts,
            dest: '<%= distDir %>/fonts/',
            flatten: true,
            filter: 'isFile'
        }]
    }
};