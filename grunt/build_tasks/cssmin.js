var config = require('../config');

module.exports = {
    vendor: {
        src: config.bundles.css.vendor,
        dest: '<%=distDir%>/css/vendor.min.css'
    }
};