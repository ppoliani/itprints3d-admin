module.exports = {
    options: {
        banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> \n' +
            '* Copyright (c) <%= grunt.template.today("yyyy") %> Brandview - Pavlos Polianidis\n' +
            '*/\n'
    },

    dev: {
        src: ['../public/js/main.js'],
        dest: '../public/js/dev/brandview.ganttchart.min.js'
    }
};