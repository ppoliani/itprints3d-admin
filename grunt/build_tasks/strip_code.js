module.exports = {
    options: {
        start_comment: 'start-test-block',
        end_comment: 'end-test-block'
    },
    src: 'dist/*.js'
};