var config = require('../config');

module.exports = {
    options: {
        stripBanners: true,
        separator: ';'
    },

    vendor: {
        src: config.bundles.js.vendor,
        dest: '<%= distDir %>/vendor.js'
    }
};