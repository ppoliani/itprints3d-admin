module.exports = {
    dev: {
        options: {
            removeComments: true,
            collapseWhitespace: true
        }
    }
};