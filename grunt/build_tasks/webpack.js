var webpack = require('webpack');

module.exports = {
    dev: {
        entry: "<%= appDir %>/core/app.js",
        output: {
            path: '<%= distDir %>',
            filename: 'bundle.js'
        },

        externals: {
            'angular': 'angular'
        },

        devtool: '#source-map'
        /*plugins: [
         new webpack.ProvidePlugin({
         angular: '../third-party/angular/angular.js'
         })
         ]*/
    }
};