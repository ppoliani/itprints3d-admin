var config = require('../config');

module.exports = {
    options: {
        style: 'expanded',
        banner: config.banner,
        compass: true,
        force: true
    },

    app: {
        files: config.bundles.sass.app
    },

    vendor: {
        files: config.bundles.sass.vendor
    },

    dist: {
        options: {
            style: 'compressed',
            compass: true
        },
        files: config.bundles.sass
    }
};