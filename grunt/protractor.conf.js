exports.config = {
    seleniumAddress: 'http://localhost:4444/wd/hub',
    specs: ['../public/js/tests/e2e/test-specs.js'],
    multiCapabilities: [{
        browserName: 'chrome'
    }]
}