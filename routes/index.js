var express  = require('express'),
    utils    = require('../utils/httpRequest'),
    router   = express.Router();

var API_HOST = 'localhost',
    API_PORT = 9999,
    METADATA_ENDPOINT = '/api/v1/breeze/metadata',
    _breezeMetadata;

/* GET home page. */
router.get('/', function(req, res) {
    _loadMetada(req, res);
});

// Support Html5 mode
router.all('/*', function(req, res) {
    _loadMetada(req, res);
});

/***
 * Loads the breeze metadata
 * @private
 */
function _loadMetada(req, res){
    if(_breezeMetadata){
        res.render('index', { dev: true, breezeMetadata: JSON.stringify(_breezeMetadata) });

        return;
    }

    var options = {
        host: API_HOST,
        path: METADATA_ENDPOINT,
        port: API_PORT
    };

    utils.request(options)
        .then(function(breezeMetadata){
            _breezeMetadata = JSON.parse(breezeMetadata);
            res.render('index', { dev: true, breezeMetadata: breezeMetadata });
        })
        .fail(function(err){
            console.log("Got error while retrieving breeze metadata: " + err.message);
            res.send(500);
        });
}

module.exports = router;
