var webpack = require('gulp-webpack'),
    concat  = require('gulp-concat'),
    uglify  = require('gulp-uglify');

// config
var webpackOptions = {

    output: {
        filename: 'bundle.min.js'
    },

    externals: {
        'angular': 'angular'
    },

    devtool: '#source-map'
};


module.exports = function(gulp, config){
    gulp.task('scripts:dist', function(){
        return gulp.src(config.appDir + '/core/app.js')
            .pipe(webpack(webpackOptions))
            .pipe(gulp.dest(config.distDir + '/'));
    });

    gulp.task('concat:dist', function(){
        return gulp.src(config.bundles.js.vendor)
            .pipe(concat('vendor.min.js'))
            .pipe(uglify())
            .pipe(gulp.dest(config.distDir + '/'));
    });
};